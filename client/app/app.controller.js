(function () {
    "use strict";
    angular.module("EMSApp")
    .controller("EmployeeController", EmployeeController)
    .controller("EditEmployeeCtrl", EditEmployeeCtrl) //declare edit Emoployee and modal ctrl here 
    //.controller("AddEmployeeCtrl", AddEmployeeCtrl) //declare addEmployee ctrl here 
    .controller("DeactivateEmployeeCtrl", DeactivateEmployeeCtrl);
    
    EmployeeController.$inject = ['EMSAppAPI', '$uibModal', '$document', '$rootScope', '$scope']; //inject dependency for modal, scope
    EditEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope']; //pick from second ctrl in modal,  it is not the same as the $uibModal
    //AddEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    DeactivateEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];

    function EditEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope) { //create here second ctrl here 
        console.log("Edit Employee Ctrl");
        var self = this;
        self.items = items;
        console.log(items);

        EMSAppAPI.getEmployee(items).then(function(result) {
            console.log(result.data);
            self.employee =  result.data;
         });

        self.saveEmployee = saveEmployee;
     
        function saveEmployee() {
            console.log("Saving... ");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.birth_date);
            console.log(self.employee.hire_date);
           
            EMSAppAPI.updateEmployee(self.employee).then(function(result){ //update here
                console.log(result);
                $rootScope.$broadcast('updateEmployeeList'); //$rootScope for listening to changes (Scopes provide separation between the model and the view, via a mechanism for watching the model for changes)
            }).catch(function(error){
                console.log(error);
            })

            $uibModalInstance.close(self.run);
        }

    }

    function DeactivateEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope) {
        console.log("Delete starting...");
        var self = this;
        console.log(items);

        EMSAppAPI.getEmployee(items).then(function(result) {
            console.log(result.data);
            self.employee =  result.data;
         });

        self.deactivateEmployee = deactivateEmployee;

        function deactivateEmployee() {
            console.log("Delete... ");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.birth_date);
            console.log(self.employee.hire_date);
            console.log(self.employee.gender);
           
            EMSAppAPI.deleteEmployee(self.employee.emp_no).then(function(result){ //update here to match service.js
                console.log(result);

                $rootScope.$broadcast('updateEmployeeList'); 
                $uibModalInstance.close(self.run);

            }).catch(function(error){
                console.log(error);
            })

            
        }

    } 

    function EmployeeController(EMSAppAPI, $uibModal, $document, $scope, $rootScope) { //add scope here
        var self = this;
        //self.format = "hh:mm:ss";

        self.employees = []; 

        self.maxsize = 5; //max size of pagination 1-5 (only for use in html, not for service or app.js)
        self.itemsPerPage = 30;
        self.totalItems = 0; //initialize the total items to 0
        self.currentPage = 1;

        self.searchEmployees = searchEmployees;
        self.addNewEmployee = addNewEmployee;
        self.editEmployee = editEmployee;
        self.deactivateEmployee = deactivateEmployee; 
        self.pageChanged = pageChanged;

        //pagination
        function pageChanged() {
            console.log("Page changed " + self.currentPage);
            searchAllEmployees(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        function searchAllEmployees(searchKeyword, orderby, itemsPerPage, currentPage) {
                EMSAppAPI.searchEmployees(searchKeyword, orderby, itemsPerPage, currentPage).then(function(results){ 
                    console.log(results);
                    self.employees = results.data.rows;
                    self.totalItems = results.data.count;
                }).catch(function (error) {
                    console.log(error);
                }); 
        }

        //watch for changes after edit (update has been made)
        //this is root scope so you can move it to other files
        $scope.$on("updateEmployeeList", function() { //code this so that the change is picked up (listen)
            console.log("Refresh employee list " + self.searchKeyword);

            searchAllEmployees(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage); 

        });

        
        function searchEmployees() {
            console.log("Search employees... "); //get
            console.log(self.orderby);

            searchAllEmployees(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage); 
        }

                                                             // add emp_no here
        function editEmployee(emp_no, size, parentSelector){ //these parameters are from bootstrap directive modal
            console.log("Edit employee...");
            console.log("Employee number is " + emp_no);

           /* EMSAppAPI.getEmployee(emp_no).then(function(result){
                console.log(result.data);
                self.item = result.data;  //this item, is from below (resolve...)
            }) */

        var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled, // --> this is employee ctrl 
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'editEmployee.html', //change
            controller: 'EditEmployeeCtrl', //change
            controllerAs: 'ctrl', //remove $ --> ' ' this is edit ctrl 
            size: size,
            appendTo: parentElem,
            resolve: {
              items: function () {
                return emp_no;
              }
            }
          }).result.catch(function(resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)
                throw resp; 
          });
        }


    function addNewEmployee() {
            console.log("Saving... ");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.birth_date);
            console.log(self.employee.hire_date);
            console.log(self.employee.gender);

            self.status = {
                message: ""
            }; 

            //document.location.replace('http://localhost:4000/#!/home');
               
            EMSAppAPI.addEmployee(self.employee).then(function(result){ //update here to match service.js
                    console.log(result);
                    $rootScope.$broadcast('newAddedEmployee', {emp_no: result.data.emp_no}); //pass in json, but choose only a specific value, eg. emp_no
                    self.status.message = "Successfully added " + self.employee.first_name + " " + self.employee.last_name;
                }).catch(function(error){
                    console.log(error);
                    self.status.message = "Failed to add";
                })

        //watch for changes after adding a new record 
        //this is root scope so you can move it to other files
        $scope.$on("newAddedEmployee", function(event, args) { //ignore event, args (json object) emp_no
            //console.log("Refresh employee list " + self.employee.first_name);
            console.log(args.emp_no);
            console.log(event);
            var employees = []; //array because of ng-repeat
            
            EMSAppAPI.getEmployee(args.emp_no).then(function(result){ 
                employees.push(result.data);
                console.log(result.data);
                self.employees = employees;
            }).catch(function (error) {
                console.log(error);
            }); 
        });
    
  

    }
    

    function deactivateEmployee(emp_no, size, parentSelector){ 
        console.log("Deactivate employee...");
        console.log("Employee number is " + emp_no);

    var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    var modalInstance = $uibModal.open({
        animation: self.animationsEnabled, // --> this is employee ctrl 
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'deactivateEmployee.html', //change
        controller: 'DeactivateEmployeeCtrl', //change
        controllerAs: 'ctrl', //addEmployee ctrl 
        size: size,
        appendTo: parentElem,
        resolve: {
          items: function () {
            return emp_no;
          }
        }
      }).result.catch(function(resp) {
        if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)
            throw resp; 
      });
    }

}

})();