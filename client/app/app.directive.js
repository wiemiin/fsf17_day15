(function() {
    angular.module('EMSApp') 
    .directive("myCurrentTime", myCurrentTime); //this naming "myCurrentTime" to match html my-current-time (all lowercase with dash)
    
    function myCurrentTime(dateFilter) { //dateFilter is from library eg. date: medium
                                                 //must return
        return function(scope, element, attrs) { //fix names for these 3 parameters (attrs is attributes), do not need $
                                                 //scope (no injection needed in directive)
                                                 //element from html <tag> (<span my-current-time  ></span>)
                                                 //attrs =" ", ctrl.format is attrs value
            console.log("Clock is working");
            var format;

                                                               //attrs.myCurrentTime calls ctrl.format
            scope.$watch(attrs.myCurrentTime, function(value){ //listen to whatever changes to this DOM
                format = value; //value from html, to store a copy in this file as format. this is to resuse and stabilze the format while the codes below is working (var format is declare outside so it's accessible by funcitons below)
                updateTime();
            });

            function updateTime(){
                var date = dateFilter(new Date(), format); //dateFilter formats the date 
                element.text(date); //element of DOM
            }

            function updateLater() {
                setTimeout(function(){
                    updateTime();
                    updateLater();
                },1000)
            }
            updateLater();

        }
    }
}()); //directive is a constructor, not IIFE
//directive can only accept libraries, customization from controller (self.format = "hh:mm:ss")

//{{}} -- call data
//ctrl.format -- pass down to directive