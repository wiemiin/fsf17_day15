(function () {  

angular
.module("EMSApp")
.config(uirouterAppConfig);
uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];


function uirouterAppConfig($stateProvider, $urlRouterProvider){

$stateProvider
    .state("home",{ //localhost:3000/#!/A 
        url : '/home',
        templateUrl: "home.html",
        controller : 'EmployeeController', //Ok to use different controller (1 page 1 controller)
        controllerAs : 'ctrl'
    })
    .state("add", {
        url: "/add",
        templateUrl: "addEmployee.html",
        controller : 'EmployeeController',
        controllerAs : 'ctrl'
    })

    $urlRouterProvider.otherwise("/home"); //if can't find any route, it goes to A 

}

})();