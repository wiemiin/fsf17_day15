(function() {
    angular
    .module("EMSApp")
    .service("EMSAppAPI", [
        '$http', 
        '$state',
        EMSAppAPI
    ]);

    function EMSAppAPI($http,$state) { 
        var self = this;

        self.searchEmployees = function(keyword_value, sortby, itemsPerPage, currentPage) {

            return $http.get(`/api/employees?keyword=${keyword_value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
            //return $http.get("/api/employees?keyword=" + keyword_value); //(3) because of $http it calls the get
        }                           //MULTIPLE ?keyword1=" + value1 + "&keyword2=" + value2

        self.getEmployee = function(emp_no) { //create an end point in app.js
            console.log(emp_no);
            return $http.get("/api/employees/" + emp_no) //this is parameter (check app.js)
        }

        self.updateEmployee = function(employee) { 
            console.log(employee);
            return $http.put("/api/employees", employee);
        }

        self.addEmployee = function(employee) {
            return $http.post("/api/employees", employee);
        }

        self.deleteEmployee = function(emp_no) {
            console.log(emp_no);                             //PARAMETERIZE means xx/xx/ 
            return $http.delete("/api/employees/" + emp_no); //parameterize it on the URL 
        }
    }

})();