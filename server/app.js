
"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require ("sequelize");


var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

const NODE_PORT = process.env.PORT || 4000;
const EMPLOYEE_API = "/api/employees"; //reuse below

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="fsf123"
var connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Employees = require('./models/employees')(connection, Sequelize);
// Sequelize calling ends here 


app.use(express.static(__dirname + "/../client")); 

console.log(__dirname + "/../client"); 

//const EMPLOYEE_API = "/api/employees"; //just a reminder

//create 
app.post(EMPLOYEE_API, function(req,res){ //get values from front end 
    console.log(">>>" + JSON.stringify(req.body)); //POST, PUT body //GET, DELETE params and query
    
    var employee = req.body;
    employee.birth_date = new Date(employee.birth_date);
    employee.hire_date = new Date(employee.hire_date);

    Employees.create(employee).then(function(result){ 
        console.log(result);
        res.status(200).json(result);
      }).catch(function(error){
          res.status(500).json(error);
      });

});

//update
app.put(EMPLOYEE_API, function(req,res){ 
    console.log(">>>" + JSON.stringify(req.body)); //POST, PUT body //GET, DELETE params and query

    console.log("Update employee... " + req.body); //body
    var emp_no = req.body.emp_no; //body
    console.log(emp_no);
    var whereClause = {limit: 1, where: {emp_no: emp_no}};
    Employees.findOne(whereClause).then(function(result){
        result.update(req.body);
        res.status(200).json(result); 
        
    }).catch(function(error){
        res.status(500).json(error);
    });
});

//delete 
//MUST MATCH service.js "/api/employees"  /:emp_no 
//$http.delete("/api/employees/" + emp_no)

app.delete(EMPLOYEE_API+"/:emp_no", function(req,res){ 

    console.log(req.params.emp_no); //POST, PUT body //GET, DELETE params and query
    
    var emp_no = req.params.emp_no;
    console.log(emp_no);

    var whereClause = {limit: 1, where: {emp_no: emp_no}};

    Employees.findOne(whereClause).then(function(result){
        result.destroy();
        res.status(200).json({}); //no record to pass back
        
    }).catch(function(error){
        res.status(500).json(error);
    });
});


//retrieve
//query is not exact match 
//$http.get("/api/employees?keyword=" + keyword_value)
app.get(EMPLOYEE_API, function(req,res){  //(4) from service the get comes to here 
    console.log("Search >" + req.query.keyword); //this value must match query? key ("keyword") from service
    var keyword = req.query.keyword; //POST, PUT body //GET, DELETE params and query
    var sortby = req.query.sortby;  
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    
    
    if(sortby === 'null') {
        sortby = "ASC";
    };

    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);

                                            //undefined, empty string and not null is not the same     empty |  Sarah | Delete Sarah 
    console.log(keyword !== 'undefined');   //string undefinited                                          F       T           T
    console.log(keyword !== undefined);     //object undefinited                                          T       T           T
    console.log(!keyword);                  //empty string is NOT null                                    F       F           T
    console.log(keyword.trim().length > 0); //empty string has 0 length (but has a space in memory)       T       T           F           
    //trim removes white space in string 

    //empty is undefined (with length)
    //Sarah has length 
    //Delete Sarah is null (without length)


    var whereClause = {offset: offset, limit: itemsPerPage,  order: [['first_name', sortby],['last_name', sortby]]};
    //offset first, then limit
    //if NOT a search keyword then return all (use above whereClause)
 

    const Op = Sequelize.Op;

    //if it's a word and has length (Sarah), then return all results on Sarah based on new whereClause 
    if ((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0) { 
            console.log(">" + keyword);
            //this searches database (uses model/employees file)
            //whereClause = {limit: 20, where: {first_name: keyword}}; 
            //SEARCH KEYWORD = first_name
           whereClause = {offset: offset, limit: itemsPerPage, order: [['first_name', sortby],['last_name', sortby]], where: {first_name: keyword}}; 
            //SEARCH KEYWORD = first_name OR last_name
            /*
            whereClause = {offset: offset, limit: itemsPerPage, order: [['first_name', sortby],['last_name', sortby]], where: {
                [Op.or]: [
                    {first_name: {
                        [Op.like]: keyword}
                    },
                    {last_name: {
                        [Op.like]: keyword}
                    },
                        ]
                    }};*/
        } 

    Employees.findAndCountAll(whereClause).then(function(results){ //find all records and count all records 
        res.status(200).json(results); // return result from database -> service -> controller -> html
        console.log(results);
    }).catch(function(error){
        res.status(500).json(error);
    }); 

});

//retrieve
app.get(EMPLOYEE_API + "/:emp_no", function(req,res){ //end point ( /:emp_no) from service here 
    console.log("One employee " + req.params.emp_no); //POST, PUT body //GET, DELETE params and query
    
    var emp_no = req.params.emp_no;
    console.log(emp_no);
    var whereClause = {limit: 1, where: {emp_no: emp_no}};
    Employees.findOne(whereClause).then(function(result){
        res.status(200).json(result); // return result from database -> service -> controller -> html
        console.log(result);
    }).catch(function(error){
        res.status(500).json(error);
    });   
});


app.use(function (req, res) { //THIS IS THE LAST OF app.js 
    res.send("<h1>Page Not Found</h1>"); //json sending data to html
});

app.listen(NODE_PORT, function () { //THIS IS THE LAST LAST OF app.js 
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app; //expose to public for testing